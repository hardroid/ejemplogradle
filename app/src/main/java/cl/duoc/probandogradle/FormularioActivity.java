package cl.duoc.probandogradle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FormularioActivity extends AppCompatActivity {

    @BindView(R.id.txt1) EditText txt1;
    @BindView(R.id.txt2) EditText txt2;
    @BindView(R.id.txt3) EditText txt3;
    @BindView(R.id.txt4) EditText txt4;
    @BindView(R.id.txt5) EditText txt5;
    @BindView(R.id.txt6) EditText txt6;
    @BindView(R.id.txt7) EditText txt7;
    @BindView(R.id.txt8) EditText txt8;
    @BindView(R.id.txt9) EditText txt9;
    @BindView(R.id.txt10) EditText txt10;
    @BindView(R.id.txt11) EditText txt11;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        ButterKnife.bind(this);

    }
}
