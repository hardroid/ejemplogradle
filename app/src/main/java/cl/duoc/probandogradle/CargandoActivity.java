package cl.duoc.probandogradle;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mikhaellopez.circularfillableloaders.CircularFillableLoaders;

public class CargandoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cargando);

        CircularFillableLoaders circularFillableLoaders = (CircularFillableLoaders)findViewById(R.id.circularFillableLoaders);
// Set Progress
        circularFillableLoaders.setProgress(60);
// Set Wave and Border Color
//        circularFillableLoaders.setColor(Color.RED);
// Set Border Width
        circularFillableLoaders.setBorderWidth(10 * getResources().getDisplayMetrics().density);
// Set Wave Amplitude (between 0.00f and 0.10f)
        circularFillableLoaders.setAmplitudeRatio(0.08f);
    }
}
